/*
	Order
	totalAmount - number,
	purchasedOn - date
			     default: new Date(),
	userId - string
	products - [
		{
			productId - string,
			quantity - number
		}
	]
*/

const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	totalAmount: {
		type: Number,
		required: [true, "Order Total Amount is required"]
	},

	purchasedOn: {
		type: Date,
		default: new Date()
	},

	status: {
		type: String,
		default: "pending"
	},

	userId: {
		type: String,
		required: [true, "Order User Id is required"]
	},

	products: [
		{
			productId: {
				type: String,
				required: [true, "Order Product Id is required"]
			},
			
			quantity: {
				type: Number,
				required: [true, "Order Product quantity is required"]
			}
		}
	]
});

module.exports = mongoose.model("Order", orderSchema);
