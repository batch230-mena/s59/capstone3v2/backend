/*
	Product
	name - string,
	description - string,
	price - number,
	stocks - number,
	isActive - boolean
			   default: true,
	createdOn - date
				default: new Date()
*/

const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product Name is required"]
	},

	description: {
		type: String,
		required: [true, "Product Description is required"]
	},

	price: {
		type: Number,
		required: [true, "Product Price is required"]
	},

	stocks: {
		type: Number,
		required: [true, "Product Stocks is required"]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("Product", productSchema);
