const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orderControllers.js");
const auth = require("../auth.js");

// Stretch Goal - Retrieve authenticated user’s orders
//router.get("/:userId", auth.verify, (request,response) => 
router.get("/", auth.verify, (request,response) => 
{
	const newData = {
		id: auth.decode(request.headers.authorization).id
	}

//	orderControllers.getUserOrders(request.params.userId, newData)
	orderControllers.getUserOrders(newData)
	.then(resultFromController => {response.send(resultFromController)})
})

// Stretch Goal - Retrieve all orders (Admin only)
router.get("/orders/all", auth.verify, (request, response) => 
{
	const allData = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	orderControllers.getAllOrders(allData)
	.then(resultFromController => response.send(resultFromController))
})


module.exports = router;