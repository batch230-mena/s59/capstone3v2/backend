const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");

// For Capstone3 - Check if email exists
router.post("/checkEmail", userControllers.checkEmailExists);

// S42 User Registration
router.post("/register", (request, response) => {
	userControllers.registerUser(request.body)
	.then(resultFromController => response.send(resultFromController));
})

// S42 User Authentication
router.post("/login", (request, response) => {
	userControllers.loginUser(request.body)
	.then(resultFromController => response.send(resultFromController))
})

// S45 Retrieve User Details
//router.get("/:userId/userDetails", auth.verify, (request,response) =>
router.get("/details", auth.verify, (request,response) => 
{
	const newData = {
		id: auth.decode(request.headers.authorization).id
	}

//	userControllers.getUserDetails(request.params.userId, newData)
	userControllers.getUserDetails(newData)
	.then(resultFromController => {response.send(resultFromController)})
})

// Stretch Goal - Set admin user
router.patch("/:userId/admin", auth.verify, (request,response) => 
{
	const newData = {
		user: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	userControllers.setAdminUser(request.params.userId, newData)
	.then(resultFromController => {response.send(resultFromController)
	})
})

// Old checkout
// S45 Non-admin User checkout (Create Order)
router.post("/checkout", auth.verify, userControllers.checkout);

// Stretch Goal - Checkout Pay
router.patch("/:userId/pay", auth.verify, (request,response) => 
{
	const newData = {
		order: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	userControllers.checkoutPay(request.params.userId, newData)
	.then(resultFromController => {
		response.send(resultFromController)
	})
})

// For Capstone3 - Check if order exists
router.post("/checkOrder", userControllers.checkOrderExists);

// For Capstone3 - Create order without products
//router.post("/createOrder", userControllers.createOrder);
router.post("/createOrder", (request, response) => {
	userControllers.createOrder(request.body)
	.then(resultFromController => response.send(resultFromController));
})

// For Capstone3 - retrieve all users
router.get("/all", auth.verify, (request,response) => 
{
	const newData = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	userControllers.getAllUsers(newData)
	.then(resultFromController => {response.send(resultFromController)})
})

module.exports = router;
